from django.shortcuts import render,redirect
from .models import jadwalModel
from .forms import jadwalKegiatan

# Create your views here.
def home(request):
    return render(request,'pages/index.html',{})
def projects(request):
    return render(request,'pages/projects.html',{})
def background(request):
    return render(request,'pages/background.html',{})
def contacts(request):
    return render(request,'pages/contacts.html',{})
def jadwal(request):
	createJadwal = jadwalKegiatan(request.POST or None)
	jadwals = jadwalModel.objects.all()
	context= {
		'jadwals':jadwals,
		'create_jadwal':createJadwal,
	}
	if request.method == 'POST':
		if createJadwal.is_valid():
			jadwalModel.objects.create(
				tanggal 	= createJadwal.cleaned_data.get('tanggal'),
				kegiatan 	= createJadwal.cleaned_data.get('kegiatan'),
				tempat 		= createJadwal.cleaned_data.get('tempat'),
				kategori 	= createJadwal.cleaned_data.get('kategori'),
				)
	return render(request,'pages/jadwal.html',context)
def deleteJadwal(request, delete_id):
	jadwalModel.objects.filter(id=delete_id).delete()
	return redirect("tugas:jadwal")
