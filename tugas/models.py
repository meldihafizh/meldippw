from django.db import models

# Create your models here.

class jadwalModel(models.Model):
	tanggal = models.DateField()
	kegiatan = models.TextField(max_length =21)
	tempat = models.CharField(max_length=20)
	kategori = models.CharField(max_length=20)

	def __str__(self):
		return "{}. {}".format(self.id,self.kegiatan)