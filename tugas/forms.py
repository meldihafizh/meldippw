from django import forms
from . import models

##Create your forms here

class jadwalKegiatan(forms.Form):
	
	tanggal = forms.DateField(required = True,
		widget= forms.SelectDateWidget)
	kegiatan = forms.CharField(max_length = 20)
	tempat = forms.CharField(max_length=20)
	kategori = forms.CharField(max_length=20)
