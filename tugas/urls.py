from django.urls import include, path
from . import views
from django.shortcuts import render
from django.conf.urls import url

app_name= 'tugas'

urlpatterns = [
    path('',views.home,name='index'),
    path('projects/', views.projects,name=('projects')),
    path('background/', views.background ,name=('background')),
    path('contacts/',  views.contacts,name='contacts'),
    path('jadwal/',views.jadwal,name='jadwal'),
    path('delete/<int:delete_id>',views.deleteJadwal,name='deleteJadwal'),
]
